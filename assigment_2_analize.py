
# coding: utf-8

# In[23]:

import glob
import os


# In[24]:

get_ipython().magic('matplotlib inline')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from ggplot import *


# In[25]:

get_ipython().system(' ls assignment_2/')


# In[26]:

glob.glob("assignment_2/R2_L1_*.csv")


# In[27]:

rez = pd.concat(pd.read_csv(p) for p in glob.glob("assignment_2/R2_L1_*.csv")).sort_values("R2", ascending=False)
rez


# In[28]:

x = 'R2'
y = 'L1'


# In[29]:

glob.glob("assignment_2/R2_L1_test*.csv")


# In[30]:

{
    ggplot(aes(x='R2', y='L1'), data=rez)
    + geom_line() + xlab('Dispersija')
    + ylab("Regurelizacijos parametras") + ggtitle("Dispersijos priklausomybes nuo regurializacijos grafikas")
}


# In[32]:

pd.concat(pd.read_csv(p) for p in glob.glob("assignment_2/R2_L1_test*.csv")).sort_values("R2", ascending=False)


# In[ ]:



