
# coding: utf-8

# In[134]:

import glob
import os


# In[135]:

get_ipython().magic('matplotlib inline')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from ggplot import *


# In[136]:

get_ipython().system(' ls assignment_1/')


# In[137]:

glob.glob("assignment_1/R2_L1_*.csv")


# In[138]:

rez = pd.concat(pd.read_csv(p) for p in glob.glob("assignment_1/R2_L1_*.csv")).sort_values("R2", ascending=False)
rez


# In[139]:

x = 'R2'
y = 'L1'


# In[140]:

{
    ggplot(aes(x='R2', y='L1'), data=rez)
    + geom_line() + xlab('Dispersija')
    + ylab("Regurelizacijos parametras") + ggtitle("Dispersijos priklausomybes nuo regurelizacijos grafikas")
}


# In[142]:

glob.glob("assignment_1/R2_L1_test*.csv")


# In[143]:

pd.concat(pd.read_csv(p) for p in glob.glob("assignment_1/R2_L1_test*.csv")).sort_values("R2", ascending=False)


# In[ ]:




# In[ ]:



