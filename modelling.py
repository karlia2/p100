
# coding: utf-8

# In[1]:

get_ipython().magic('reload_ext autoreload')
get_ipython().magic('autoreload 2')
get_ipython().magic('matplotlib inline')


# In[2]:

import glob
import os
import subprocess
import tempfile

import IPython
import ggplot as gg
import pandas as pd

import date_group as dg
import preprocessing as prp
import util


# In[3]:

incorrect_descriptives = util.validate_descriptives(
    glob.glob("data_output/by_country_by_hour/user_country_710_*"), 
              "data_output/descriptives/by_country_by_hour")
incorrect_descriptives


# In[4]:

# pakeisti kiekvinam paleidimui
country_id = "710"
description_part = "first_4_weeks"
n_testing_periods = 10


# In[5]:

filenames = glob.glob("data_output/by_country_by_hour/user_country_{}_*".format(country_id))

dt_fn_pairs = dg.make_datetime_filename_pairs(filenames)

splits = dg.make_training_testing_datasets(
        datetime_filename_pairs=dt_fn_pairs,
        n_training_periods=4,
        training_period_type='week',
        n_testing_periods=1,
        testing_period_type='day',
        n_window_periods=1,
        window_period_type='week',
        datetime_from=None,
        datetime_to=None,
        min_testing_datasets=n_testing_periods,
        has_named_tuples=False)


# In[6]:

len(splits)


# In[7]:

# geriau nekeisti zemiau


# In[8]:

dir_output = "data_output/country_{}_{}".format(country_id, description_part)
dir_output


# In[9]:

training_filenames = splits[0][0][1]

training_descriptives_dirs = util.mk_paths_descr_from_dataset(
    training_filenames, "data_output/descriptives/by_country_by_hour")

target_counts = prp.merge_descriptives(
    training_descriptives_dirs, ["impression_was_clicked"]
)["impression_was_clicked"]

n_neg, n_pos = target_counts["0"], target_counts["1"]

prob_neg_subsampling = prp.calc_prob_neg_sampling(n_neg, n_pos, 0.1)


# ## Modeliavimas

# In[15]:

VW_HOME = os.path.join(os.path.expanduser("~"), "vowpal_wabbit")
PATH_VW_UTL = os.path.join(VW_HOME, 'utl')
PATH_VW_HS = os.path.join(PATH_VW_UTL, "vw-hypersearch")


# In[16]:

_, tmp_path_training_files = tempfile.mkstemp()

with open(tmp_path_training_files, "w") as f:
    f.write(" ".join(training_filenames))

path_train = "tmp_train.vw"
path_validation = "tmp_validation.vw" 

os.makedirs(dir_output, exist_ok=True)

hs_split_pipe_parts = [
    "cat {}".format(tmp_path_training_files),
    "./cat_csv",
    "./downsample --min_pos_ratio=0.1 {} {}".format(n_pos, n_neg),
    "./csv_2_vw",
    "./split --seed=2000 --probability=0.7 {} {}".format(path_train, path_validation)
]

hs_split_cmd = " | ".join(hs_split_pipe_parts)
    
hs_training_vw_cmd = " ".join([
        "vw",
        "--loss_function logistic",
        "-f {}/serialized.model".format(dir_output),
        "--cache",
        "--passes 20",
        "--l1 %",
        "--readable_model {}/auto_model.txt".format(dir_output),
        "-d {}".format(path_train)
    ])

hs_cmd = " ".join([
        PATH_VW_HS,
        "-t {}".format(path_validation),
        "-L",
        "0.000000001",
        "0.001",
        hs_training_vw_cmd,
        "> {}/best_L1_reg.txt 2>&1; rm *.cache;".format(dir_output)
    ])


# #### Modeliavimo vykdymas

# In[17]:

subprocess.call(hs_split_cmd, shell=True, executable="/bin/bash")


# In[18]:

subprocess.call(hs_cmd, shell=True, executable="/bin/bash")


# In[19]:

os.remove(tmp_path_training_files)


# ## Modelio įvertinimas

# In[20]:

testing_datasets = splits[0][1][:n_testing_periods]


# #### Prognozavimas

# In[21]:

for i, (interval, fnames) in enumerate(testing_datasets):
    testing_vw_cmd = " ".join([
            "vw",
            "--loss_function logistic",
            "-i data_output/baseline.model",
            "--testonly",
            "-p {}/predictions_{}.txt".format(dir_output, i)
        ])
    
    testing_pipe_parts = [
            "echo {}".format(" ".join(fnames)),
            "./cat_csv",
            "./csv_2_vw",
            testing_vw_cmd
        ]
    
    testing_pipe = " | ".join(testing_pipe_parts)
    subprocess.call(testing_pipe, shell=True)


# #### Metrikų skaičiavimas

# In[22]:

for i, (interval, fnames) in enumerate(testing_datasets):
    evaluation_pipe_parts = [
            "echo {}".format(" ".join(fnames)),
            "./cat_csv",
            "./extract_column impression_was_clicked",
            "./mk_eval_input {}/predictions_{}.txt".format(dir_output, i),
            "./eval_model > {}/metrics_{}.txt".format(dir_output, i)
        ]
    
    evaluation_pipe = " | ".join(evaluation_pipe_parts)
    subprocess.call(evaluation_pipe, shell=True)


# #### Metrikų apjungimas

# In[23]:

metrics_dfs = [
    ("{}/metrics_{}.txt".format(dir_output, i), 
     pd.read_csv('{}/metrics_{}.txt'.format(dir_output, i), sep="\t", 
                 index_col=0, header=None).T,)
    for i in range(len(testing_datasets))
]

for i, (p, df) in enumerate(metrics_dfs):
    df["i_test_period"] = i + 1

metrics_df = pd.concat(df for _, df in metrics_dfs).reset_index(drop=True)
metrics_df.to_csv(os.path.join(dir_output, "metrics.csv"), index=None)


# In[24]:

metrics_df


# In[25]:

metrics_long_df = pd.melt(
    metrics_df, 
    id_vars=["i_test_period"], 
    value_vars=["PRE", "REC", "PRF", "AUC"], 
    var_name="variable")


# In[26]:

metrics_long_df


# In[27]:

(
    gg.ggplot(metrics_long_df, gg.aes(x="i_test_period", y="value", color="variable")) + 
    gg.geom_line() + 
    gg.xlab("$N$ days after training") + 
    gg.ggtitle("Metrics") +
    # uzkomentuoti jei nereikia tuscios vietos
    gg.ylim(0, 1) +
    gg.theme_bw()
)


# In[ ]:



